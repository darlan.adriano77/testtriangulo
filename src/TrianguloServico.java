
public class TrianguloServico {

	public TipoEnum calcularTriangulo(Triangulo triangulo) {

			
			try {
			    if (triangulo.getMedida1() + triangulo.getMedida2() <= triangulo.getMedida3() || triangulo.getMedida1() + triangulo.getMedida3() <= triangulo.getMedida2() || triangulo.getMedida2() + triangulo.getMedida3() <= triangulo.getMedida1()) {
			    	IllegalArgumentException argumentException = new IllegalArgumentException();
			    	excecaoTriangulo(argumentException , triangulo);
			        throw argumentException;
			    }

				if (triangulo.getMedida1() == triangulo.getMedida2() && triangulo.getMedida2() == triangulo.getMedida3()) {
					return TipoEnum.EQUILATERO;

				} else if (triangulo.getMedida1() == triangulo.getMedida2() || triangulo.getMedida1() == triangulo.getMedida3() || triangulo.getMedida2() == triangulo.getMedida3()) {
					return TipoEnum.ISOSCELES;

				} else {
					return TipoEnum.ESCALENO;
				}

			} catch (IllegalArgumentException e) {
				throw e;
			}

	}
	private void excecaoTriangulo(IllegalArgumentException argumentException , Triangulo triangulo) {
		
		if (triangulo.getMedida1() == 0 && triangulo.getMedida2() == 0 && triangulo.getMedida3() == 0) {
			argumentException = new IllegalArgumentException("Todos os lados iguais a 0 ");
		} else if (triangulo.getMedida1() == 0 || triangulo.getMedida2() == 0 || triangulo.getMedida3() == 0) {
			argumentException = new IllegalArgumentException("Um dos lados iguais a 0");
		} else if (triangulo.getMedida1() < 0 || triangulo.getMedida2() < 0 || triangulo.getMedida3() < 0) {
			argumentException = new IllegalArgumentException("Um ou mais lados com valores negativos");
		} else if (triangulo.getMedida1() + triangulo.getMedida2() == triangulo.getMedida3() || triangulo.getMedida1() + triangulo.getMedida3() == triangulo.getMedida2() || triangulo.getMedida2() + triangulo.getMedida3() == triangulo.getMedida1()) {
			argumentException = new IllegalArgumentException("Soma de dois Lados igual ao terceiro ");
		} else if (triangulo.getMedida1() + triangulo.getMedida2() < triangulo.getMedida3() || triangulo.getMedida1() + triangulo.getMedida3() < triangulo.getMedida2() || triangulo.getMedida2() + triangulo.getMedida3() < triangulo.getMedida1()) {
			argumentException = new IllegalArgumentException("Soma de dois dos lados menor que o terceiro lado");
		}
	}

}
