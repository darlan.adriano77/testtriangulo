public class Triangulo {
	
	private Integer medida1;
	
	private Integer medida2;
	
	private Integer medida3;

	public Triangulo() {
	}
	
	public Integer getMedida1() {
		return medida1;
	}
	public void setMedida1(Integer medida1) {
		this.medida1 = medida1;
	}
	public Integer getMedida2() {
		return medida2;
	}
	public void setMedida2(Integer medida2) {
		this.medida2 = medida2;
	}
	public Integer getMedida3() {
		return medida3;
	}
	public void setMedida3(Integer medida3) {
		this.medida3 = medida3;
	}

}
