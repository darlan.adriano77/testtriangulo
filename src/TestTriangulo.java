import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestTriangulo {
	
	TrianguloServico trianguloServico = new TrianguloServico();
	//Triângulo escaleno válido
	@Test
	public void teste1() {
		
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(2);
		triangulo.setMedida2(3);
		triangulo.setMedida3(4);
		
		assertEquals(TipoEnum.ESCALENO, trianguloServico.calcularTriangulo(triangulo));
	}

	//Triângulo isósceles válido 
	@Test
	public void teste2() {
		
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(12);
		triangulo.setMedida2(16);
		triangulo.setMedida3(12);
		assertEquals(TipoEnum.ISOSCELES, trianguloServico.calcularTriangulo(triangulo));
	}
	
	//Triângulo equilatero válido
	@Test
	public void teste3() {
		
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(20);
		triangulo.setMedida2(20);
		triangulo.setMedida3(20);
		assertEquals(TipoEnum.EQUILATERO, trianguloServico.calcularTriangulo(triangulo));
	}
	
	//Pelo menos 3 casos de teste (CTs) para isósceles válido contendo a permutação dos mesmos valores
	@Test
	public void teste4() {
		
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(17);
		triangulo.setMedida2(11);
		triangulo.setMedida3(17);
		assertEquals(TipoEnum.ISOSCELES, trianguloServico.calcularTriangulo(triangulo));
	}
	@Test
	public void teste5() {
		
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(11);
		triangulo.setMedida2(17);
		triangulo.setMedida3(17);
		assertEquals(TipoEnum.ISOSCELES, trianguloServico.calcularTriangulo(triangulo));
	}
	@Test
	public void teste6() {
		
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(17);
		triangulo.setMedida2(17);
		triangulo.setMedida3(11);
		assertEquals(TipoEnum.ISOSCELES, trianguloServico.calcularTriangulo(triangulo));
	}
	
	
	//Um valor zero
	@Test(expected = IllegalArgumentException.class)
	public void teste7()  {
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(20);
		triangulo.setMedida2(20);
		triangulo.setMedida3(0);
		trianguloServico.calcularTriangulo(triangulo);
	}

	//Um valor negativo
	@Test(expected = IllegalArgumentException.class)
	public void teste8() {
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(20);
		triangulo.setMedida2(20);
		triangulo.setMedida3(-12);
		trianguloServico.calcularTriangulo(triangulo);
	}

	//A soma de 2 lados é exatamente igual ao terceiro lado
	@Test(expected = IllegalArgumentException.class)
	public void teste9() {
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(8);
		triangulo.setMedida2(8);
		triangulo.setMedida3(16);
		trianguloServico.calcularTriangulo(triangulo);
	}
	
	//Para o item acima, um CT para cada permutação de valores
	@Test(expected = IllegalArgumentException.class)
	public void teste10() {
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(8);
		triangulo.setMedida2(16);
		triangulo.setMedida3(8);
		trianguloServico.calcularTriangulo(triangulo);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void teste11() {
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(16);
		triangulo.setMedida2(8);
		triangulo.setMedida3(8);
		trianguloServico.calcularTriangulo(triangulo);
	}

	

	//CT em que a soma de 2 lados é menor que o terceiro lado
	@Test(expected = IllegalArgumentException.class)
	public void teste12() {
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(8);
		triangulo.setMedida2(6);
		triangulo.setMedida3(20);
		trianguloServico.calcularTriangulo(triangulo);
	}

	//Para o item acima, um CT para cada permutação de valores
	@Test(expected = IllegalArgumentException.class)
	public void teste13() {
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(8);
		triangulo.setMedida2(20);
		triangulo.setMedida3(6);
		trianguloServico.calcularTriangulo(triangulo);
	}
	@Test(expected = IllegalArgumentException.class)
	public void teste14() {
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(20);
		triangulo.setMedida2(8);
		triangulo.setMedida3(6);
		trianguloServico.calcularTriangulo(triangulo);
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void teste15() {
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(8);
		triangulo.setMedida2(6);
		triangulo.setMedida3(20);
		trianguloServico.calcularTriangulo(triangulo);
	}
	

	//Um CT para os três valores iguais a zero
	@Test(expected = IllegalArgumentException.class)
	public void teste16() {
		Triangulo triangulo = new Triangulo();
		triangulo.setMedida1(0);
		triangulo.setMedida2(0);
		triangulo.setMedida3(0);
		trianguloServico.calcularTriangulo(triangulo);
	}

}




